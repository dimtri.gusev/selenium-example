import pytest
from selenium.webdriver import ChromeOptions, Chrome

from example1.utility import make_attachment
options = ChromeOptions()
options.add_argument("--headless=new")
options.add_argument('--no-sandbox')
driver = Chrome(options=options)

def pytest_exception_interact(node):
    browser = node.funcargs.get('browser')
    if browser:
        make_attachment(browser)


@pytest.fixture
def browser():
    driver.maximize_window()
    yield driver
    driver.quit()
